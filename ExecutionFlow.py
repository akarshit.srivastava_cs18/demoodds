# -*- coding: utf-8 -*-
from Megan.MainClasses import RegularSpinStep, FreeSpinStep
import Megan.BaseSteps as BaseSteps
import Megan.MainClasses
from Megan.MainClasses import ReelStops
from Megan.SlotEvaluator import SlotEvaluator
from copy import copy
import Megan.utils
from Megan.CosmicGemsWinStreak.BonusRespins import BonusReSpins
from Megan.CosmicGemsWinStreak.BonusFreespins import BonusFreeSpins
from Megan.CosmicGemsWinStreak.__init__ import CosmicGemsWinStreak


class InitializeGameVariables(RegularSpinStep):
    def process(self, vo_spin, state, context):
        vo_spin.isRespin = False
        vo_spin.isRespinRetrigger = False
        vo_spin.freespinTriggered = False
        context.baseScatterWin = 0


class FeatureAndReelsSelectionMain(RegularSpinStep):
    def process(self, vo_spin, state, context):
        context.selectedReelsKey = 'bg_reels'
        context.selectedReels = context.config.reels[context.selectedReelsKey]


class FeatureAndReelsSelectionFreeSpins(FreeSpinStep):
    def process(self, vo_spin, state, context):
        context.selectedReelsKey = 'fs_reels'
        if vo_spin.isBaseRespin:
            context.selectedReelsKey = 'bg_reels'
        context.selectedReels = context.config.reels[context.selectedReelsKey]


# To generate 3x5 grid inside base game.
class RaffleSelectedReels(BaseSteps.RaffleSelectedReels, RegularSpinStep): pass


# In free spins, Respin and Respin inside free spins,  grid is of 5 X 4 so rowCount = 4
class RaffleSelectedReelsFreeSpin(FreeSpinStep):
    def process(self, vo_spin, state, context):
        context.randomSuply = ReelStops(context.selectedReels, [None, None, None, None, None], vo_spin.debug)
        # layoutRowCount changed to layoutRowCountFreeSpin
        context.reelLayout = SlotEvaluator.generateReelLayout(context.selectedReels, context.randomSuply, rowCount=4)
        context.reelLayoutMath = copy(context.reelLayout)


# calculating winning lines for basegame with layoutRowcount = 3
class CalculateWinningLinesAllWays(BaseSteps.CalculateWinningLinesAllWays, RegularSpinStep): pass


# Check base game Re-spin Feature
# Add Re trigger if Re-spin Winnings are seen
class ReSpinsTrigger(RegularSpinStep):
    def process(self, vo_spin, state, context):
        freespinCount = 0
        if context.winningLines.creditsWon:
            vo_spin.isRespin = True
            vo_spin.isBaseRespin = True
            respin_number = context.config.re_spin['award']['initialSpin']
            vo = BonusReSpins(CosmicGemsWinStreak, vo_spin, state, context, respin_number,
                              triggeringSpin='baseReSpin').vo_Return
            self.__dict__ = vo.__dict__
            self.featureTrigger = 'Respin'
            self.bonusName = 'Respin'
            # added for client end purpose
            self.baseTier = vo.__dict__['ReSpinList'][-1][0]['baseTier']
            self.reSpinWin = vo.__dict__['ReSpinList'][-1][0]['reSpinWin']
            self.totalRespins = len(vo.__dict__['ReSpinList'])
            self.baseSpin = len(vo.__dict__['ReSpinList'])
            self.freeSpin = 0
            freespinCount = vo.__dict__['freeSpinNode']['freeSpinsRemaining']

        vo_spin.isRespinRetrigger = False
        vo_spin.isRespin = False
        vo_spin.FreespinsFrmRespins = freespinCount
        if vo_spin.FreespinsFrmRespins:
            vo_spin.freespinTriggered = True


# Add FreeSpins class
# Add re-trigger
# Add ReSpin In FreeSpins
class TriggerFreeSpins(RegularSpinStep):
    def process(self, vo_spin, state, context):
        if vo_spin.isRespin is True or vo_spin.isRespinRetrigger is True:
            return
        scatter_sym = context.payTable.getSymbol('scatter')
        match_positions = Megan.utils.findMatchPositions(context.reelLayoutMath, scatter_sym)
        sym_count = len(match_positions)  # find len of a list
        if sym_count >= 3 or vo_spin.freespinTriggered:
            vo_spin.isBaseRespin = False
            multiplier = context.scatterMultiplier if hasattr(context, 'scatterMultiplier') else 1
            free_spin_number = 0
            if sym_count >= 3:
                free_spin_number = context.config.free_spins_dict[sym_count]
                context.baseScatterWin = context.payTable.getPay(12, sym_count) * vo_spin.totalBet * multiplier

            totFreeSpins = vo_spin.FreespinsFrmRespins + free_spin_number
            vo = BonusFreeSpins(CosmicGemsWinStreak, vo_spin, state, context, totFreeSpins).vo_Return
            self.__dict__ = vo.__dict__
            self.baseFreespinsCount = free_spin_number
            self.bonusName = self.__class__.__name__
            self.symbol = scatter_sym
            self.matchPositions = match_positions
            self.featureTrigger = 'Freespin'
            self.totalFreeSpins = len(vo.__dict__['freeSpinList'])


# calculating winning lines for freegame with layoutRowcount = 4
class CalculateWinningLinesAllWaysFreeSpin(FreeSpinStep):
    def process(self, vo_spin, state, context):
        multiplier = context.linesMultiplier if hasattr(context, 'linesMultiplier') else 1
        context.winningLines = SlotEvaluator.getWinningLinesAllWays(paysWildLines=context.config.paysWildLines,
                                                                    payTable=context.payTable,
                                                                    layout=context.reelLayoutMath,
                                                                    layoutRowCount=context.config.layoutRowCountFreeSpin,
                                                                    betPerLine=vo_spin.betPerLine,
                                                                    multiplier=multiplier)


class ReSpinsInFreeSpinTrigger(FreeSpinStep):
    def process(self, vo_spin, state, context):

        if vo_spin.isRespin is True or vo_spin.isRespinRetrigger is True:
            return
        freespinCount = 0
        if context.winningLines.creditsWon:
            vo_spin.isBaseRespin = False
            vo_spin.isRespin = True
            respin_number = context.config.re_spin['award']['initialSpin']
            vo = BonusReSpins(CosmicGemsWinStreak, vo_spin, state, context, respin_number, 'ReSpinInFreeSpin').vo_Return

            self.__dict__ = vo.__dict__
            self.bonusName = 'RespinInFreespin'
            self.featureTrigger = 'RespinInFreespin'
            self.baseTier = vo.__dict__['ReSpinList'][-1][0]['baseTier']
            self.reSpinWin = vo.__dict__['ReSpinList'][-1][0]['creditsWonAccumulated']
            self.totalRespins = len(vo.__dict__['ReSpinList'])
            freespinCount = vo.__dict__['freeSpinNode']['freeSpinsRemaining']
            self.baseSpin = vo.__dict__['ReSpinList'][-1][0]['baseSpin']
            self.freeSpin = len(vo.__dict__['ReSpinList'])
            self.creditsWon = vo.__dict__['ReSpinList'][-1][0]['creditsWonAccumulated']

        vo_spin.isRespinRetrigger = False
        vo_spin.isRespin = False
        vo_spin.FreespinsFrmRespins = freespinCount
        if vo_spin.FreespinsFrmRespins:
            vo_spin.freespinTriggered = True
            vo_spin.retriggerAmount = freespinCount
            self.retriggerSpins = vo_spin.retriggerAmount


# calculating winning lines for freegame with layoutRowcount = 4
class CalculateWinningLinesAllWaysFS(FreeSpinStep):
    def process(self, vo_spin, state, context):
        multiplier = context.linesMultiplier if hasattr(context, 'linesMultiplier') else 1
        context.winningLines = SlotEvaluator.getWinningLinesAllWays(paysWildLines=context.config.paysWildLines,
                                                                    payTable=context.payTable,
                                                                    layout=context.reelLayoutMath,
                                                                    layoutRowCount=context.config.layoutRowCountFreeSpin,
                                                                    betPerLine=vo_spin.betPerLine,
                                                                    multiplier=multiplier)


class CalculateWinningLines(BaseSteps.CalculateWinningLinesAllWays, RegularSpinStep): pass


class CalculateCreditsWon(BaseSteps.CalculateCreditsWon, RegularSpinStep, FreeSpinStep): pass


class AddBaseScattterWin(RegularSpinStep):
    def process(self, vo_spin, state, context):
        if context.baseScatterWin:
            context.creditsWon += context.baseScatterWin


class ReturnGeneratedSequence(BaseSteps.ReturnGeneratedSequence, RegularSpinStep): pass


class FlushGameState(BaseSteps.FlushGameState, RegularSpinStep): pass
